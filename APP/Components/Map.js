import React from 'react'
import {View,StyleSheet,TextInput,Text,Image, ImageBackground,Button,ScrollView, TouchableOpacity,Alert,Dimensions} from 'react-native'
import moment from 'moment'
import {connect} from 'react-redux'
import MapView,{Marker, Polyline} from 'react-native-maps'

class Map extends React.Component {
    
    constructor(props){
        super(props)
        activities = this.props.activities
        var points=[]
        for (let index = 0; index < activities.length; index++) {
            for (let b = 0; b < activities[index].points.length; b++) {
                points.push([activities[index].points[b],activities[index].pollcouleur[b],1000*index+b])
                
                
            }
            
        }
        this.points=points
    }

    render() {

        points=this.points
        
        return(
            <View>
                <MapView
                style={styles.map}
                initialRegion={{
                    latitude:48.85,
                    longitude:2.34,
                    latitudeDelta: 0.03,
                    longitudeDelta: 0.03,
                }}
                >
                {
                    points.map((element,index) => (
                            <Marker
                            key={index}
                            coordinate={element[0]}
                            
                            pinColor={element[1]}
                            />
                        )
                    

                    )
                }

                </MapView>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    map:{
        height:Dimensions.get('window').height
    }

})
const mapStateToProps = (state) => {
    return {
        activities: state.activities
  
    }
  }
  
  export default connect(mapStateToProps)(Map)
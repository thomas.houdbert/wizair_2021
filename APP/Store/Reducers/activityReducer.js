import moment from 'moment'
import GPSData from '../../Helpers/TestGPSData'

const initactis = [
    {
        id:[1,moment(new Date("2021-05-10")).isoWeek()],
        date:"2021-05-10",
        distance:8,
        average_speed:24,
        duration:20,
        min_pollution:Math.trunc(10*Math.min(...GPSData[0][1]))/10,
        max_pollution:Math.trunc(10*Math.max(...GPSData[0][1]))/10,
        average_pollution:Math.trunc(10*GPSData[0][1].reduce((a,b)=>(a+b))/GPSData[0][1].length)/10,
        desc:'Lorem ipsum si vis potes, si vis passem para belum. Tu coque fili, setum iliam batus feorum. Lior batis mafem',
        points:GPSData[0][0],
        polls:GPSData[0][1],
        pollcouleur:GPSData[0][2]
    },   
    {
        id:[2,moment(new Date("2021-05-12")).isoWeek()],
        date:"2021-05-12",
        distance:17,
        average_speed:17,
        duration:60,
        min_pollution:Math.trunc(10*Math.min(...GPSData[1][1]))/10,
        max_pollution:Math.trunc(10*Math.max(...GPSData[1][1]))/10,
        average_pollution:Math.trunc(10*GPSData[1][1].reduce((a,b)=>(a+b))/GPSData[1][1].length)/10,
        desc:'Lorem ipsum si vis potes, si vis passem para belum. Tu coque fili, setum iliam batus feorum. Lior batis mafem',
        points:GPSData[1][0],
        polls:GPSData[1][1],
        pollcouleur:GPSData[1][2]
    },
    {
        id:[3,moment(new Date("2021-06-10")).isoWeek()],
        date:"2021-06-10",
        distance:6,
        average_speed:18,
        duration:20,
        min_pollution:Math.trunc(10*Math.min(...GPSData[2][1]))/10,
        max_pollution:Math.trunc(10*Math.max(...GPSData[2][1]))/10,
        average_pollution:Math.trunc(10*GPSData[2][1].reduce((a,b)=>(a+b))/GPSData[2][1].length)/10,
        desc:'Lorem ipsum si vis potes, si vis passem para belum. Tu coque fili, setum iliam batus feorum. Lior batis mafem',
        points:GPSData[2][0],
        polls:GPSData[2][1],
        pollcouleur:GPSData[2][2]
    },
    {
        id:[4,moment(new Date("2021-06-09")).isoWeek()],
        date:"2021-06-09",
        distance:22,
        average_speed:11,
        duration:120,
        min_pollution:Math.trunc(10*Math.min(...GPSData[3][1]))/10,
        max_pollution:Math.trunc(10*Math.max(...GPSData[3][1]))/10,
        average_pollution:Math.trunc(10*GPSData[3][1].reduce((a,b)=>(a+b))/GPSData[3][1].length)/10,
        desc:'Lorem ipsum si vis potes, si vis passem para belum. Tu coque fili, setum iliam batus feorum. Lior batis mafem',
        points:GPSData[3][0],
        polls:GPSData[3][1],
        pollcouleur:GPSData[3][2]
    }
].sort(compare)

const initialState = {activities:initactis,maxid:10,objectifs:calculObjectifs(initactis),objectifsSemaine:calculObjectifsSemaine(initactis),distTot:calculAllTime(initactis)}

function compare(a,b){
    var A =a.date.split("-")
    A[0]=360*A[0]
    A[1]=30*A[1]
    const valeurA= A.reduce((i,j)=>{
        return i/1 + j/1
    })
    var B =b.date.split("-")
    B[0]=360*B[0]
    B[1]=30*B[1]
    const valeurB= B.reduce((i,j)=>{
        return i/1 + j/1
    })
    if(valeurA<valeurB){
        return 1
    }
    else{
        return -1
    }

}

function calculObjectifs(actis) {
    var i=0
    var distance=0
    var pollution=0
    const date = moment().format("DD/MM/YYYY")
    while (actis.length > i && moment(new Date(actis[i].date)).format('DD/MM/YYYY')==date) {
        distance += actis[i].distance
        pollution += actis[i].average_pollution
        i++
    }
    return({distanceJour:distance,moyPol:pollution/(i==0?1:i),nombreTrajets:i})

}

function calculObjectifsSemaine(actis) {
    var i=0
    var distance=0
    var pollution=0
    const date = moment().isoWeek()
    while (actis.length > i && actis[i].id[1]==date) {
        distance += actis[i].distance
        pollution += actis[i].average_pollution
        i++
    }
    return({distanceJour:distance,moyPol:pollution/(i==0?1:i),nombreTrajets:i})

}

function calculAllTime(actis) { 
    var somme = 0
    for(item in actis){
        somme += actis[item].distance
    }
    return(somme) 

}

function activityGestion(state = initialState, action){
    let nextState
    switch (action.type) {
        case 'ADD_ACTIVITY':
            nextState = {
                ...state,
                activities:[...state.activities,action.value].sort(compare),
                maxid:state.maxid+1,
                objectifs:calculObjectifs([...state.activities,action.value].sort(compare)),
                objectifsSemaine:calculObjectifsSemaine([...state.activities,action.value].sort(compare)),
                distTot:calculAllTime(state.activities)
            }
            return nextState || state
            

        case 'DELETE_ACTIVITY':
            nextState = {
                ...state,
                activities: state.activities.filter((item,index)=> item.id!= action.value.id),
                objectifs:calculObjectifs(state.activities.filter((item,index)=> item.id!= action.value.id)),
                objectifsSemaine:calculObjectifsSemaine(state.activities.filter((item,index)=> item.id!= action.value.id)),
                distTot:calculAllTime(state.activities)
            }
            return nextState || state

        case 'DELETE_ALL-ACTIVITY':
            nextState ={
                ...state,
                activities:[],
                objectifs:calculObjectifs([]),
                objectifsSemaine:calculObjectifsSemaine([]),
                distTot:calculAllTime(state.activities)
            }
            
            
    
        default:
            return state
    }
}

export default activityGestion